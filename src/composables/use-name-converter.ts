export default function useNameConverter() {
  const toUppercase = (name: string) => name[0].toUpperCase() + name.substr(1)

  return {toUppercase}
}
